<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GetData extends Controller
{
    public function data(){

        $show_limit=1000;
        $iterations=100000;

        $index_arr=range(0,$iterations);

        $ids=array();
        $manufacturers=array();
        $families=array();
        $codes=array();
        $scores=array();
        foreach($index_arr as $each){

            $ids[]=$each;
            if($each%2===0){
                $manufacturers[]=mt_rand(0,10);
            }else{
                $manufacturers[]='[NO MANUFACTURER]';
            }

            if($each%4===0){
                $families[]=mt_rand(0,10);
            }else{
                $families[]='[NO FAMILY]';
            }

            if($each%7===0){
                $codes[]=mt_rand(0,10);
            }else{
                $codes[]='[NO CODE]';
            }
            $scores[]=mt_rand(0,1000);
        }

        $results=array();

        for($i=0;$i<=$iterations;$i++){
            ($manufacturers[$i]=='[NO MANUFACTURER]')?$manufacturer='[NO MANUFACTURER]':$manufacturer='Manufacturer '.$manufacturers[$i];
            ($families[$i]=='[NO FAMILY]')?$family='[NO FAMILY]':$family='Family '.$families[$i];
            ($codes[$i]=='[NO CODE]')?$code='[NO CODE]':$code='Code '.$codes[$i];
            $results[]=array(
                'id'=>$ids[$i],
                'manufacturer'=>$manufacturer, 
                'family'=>$family,
                'code'=>$code,
                'score'=>'Score '.$scores[$i]
            );
        }

        array_multisort(
            $manufacturers,SORT_DESC,
            $families,SORT_DESC,
            $codes,SORT_DESC,
            $scores,SORT_DESC,
            $results
        );
        return (array_slice($results,0,$show_limit));
    }

    public function test(){
        return view('test'); 
    }
}
