
var $ = require("jquery");


$(document).ready(function(){
    var positions={};
    $.ajax({
        type:'json',
        method:'get',
        url:'/getdata',
        success:function(r){
            console.log(r);
            positions=showPositions(r);
        },
        error: function(error){
            console.log('server error: '+error);
        }
    });
    $(".search-btn").click(function(){
        var search_request=$(".search-request").val();

        serach_positions(positions,search_request);
    });

    //enter pressed
    $(".search-request").keyup(function(e){
        if(e.keyCode==13){
            var search_request=$(".search-request").val();
            serach_positions(positions,search_request);
        }
    });

    $(".reload-positions").click(function(){
        showPositions(positions);
    });
});

function showPositions(data){
    $(".data-container").empty();
    console.log(data);
    data_sorted=array_chunk(data,8);
    $.each(data_sorted,function(ci,chunk){
        var row='<ul class="flex-container">';
        $.each(chunk,function(indx,d){
            var data='<li class="flex-item">';
            data+=d['manufacturer']+'</br>';
            data+=d['family']+'</br>';
            data+=d['code']+'</br></br></br>';
            data+=d['score'];
            data+='</li>';
            row+=data;
        });
        row+='</ul>';
        $(".data-container").append(row);
    });
    return data;
}


function serach_positions(data,str){
    var search_data=[];
    $.each(data,function(i,each){
        if(each.manufacturer.includes(str) || each.family.includes(str) || each.code.includes(str) || each.score.includes(str)){
            search_data.push(each);
        }
    });
    console.log(search_data);
    showPositions(search_data);
}



function sleep(ms) {
    ms += new Date().getTime();
    while (new Date() < ms){}
} 

function array_chunk( input, size ) {
    for(var x, i = 0, c = -1, l = input.length, n = []; i < l; i++){
        (x = i % size) ? n[c][x] = input[i] : n[++c] = [input[i]];
    }
    return n;
}