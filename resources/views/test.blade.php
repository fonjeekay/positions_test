<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/app.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js"></script>
    <script src="/js/app.js"></script>
</head>
<body>
<nav class="navbar" role="navigation" aria-label="main navigation">
    <div class="navbar-brand">
        <div class="navbar-item">
            <h3 class="test">Test</h3>
        </div>
    </div>
    <div class="navbar-end">
        <div class="navbar-item">
            <div class="control">
                <input class="input search-request" type="text" placeholder="Search">
            </div>
            <div class="control">
                <a class="button is-default search-btn">
                    <i class="fas fa-search"></i>
                </a>
            </div>
            <div class="control">
                    <a class="button is-dark reload-positions"><i class="fas fa-sync"></i></a>
            </div>
        </div>
    </div>
</nav>
<div class="data-container"></div>
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
</body>
</html>